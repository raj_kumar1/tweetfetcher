package provider;

import Constants.TwitterConstants;
import factory.OAuth2TokenFactory;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.OAuth2Token;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterProvider {
    /**
     * Get a well authenticated Twitter object for making subsequent calls.
     *
     * @return	Twitter4J Twitter object that's ready for API calls
     */
    public Twitter provideTwitter()
    {
        OAuth2Token token;

        //	First step, get a "bearer" token that can be used for our requests
        token = new OAuth2TokenFactory().getOAuth2Token();

        //	Now, configure our new Twitter object to use application authentication and provide it with
        //	our CONSUMER key and secret and the bearer token we got back from Twitter
        ConfigurationBuilder cb = new ConfigurationBuilder();

        cb.setApplicationOnlyAuthEnabled(true);

        cb.setOAuthConsumerKey(TwitterConstants.CONSUMER_KEY);
        cb.setOAuthConsumerSecret(TwitterConstants.CONSUMER_SECRET);

        cb.setOAuth2TokenType(token.getTokenType());
        cb.setOAuth2AccessToken(token.getAccessToken());

        return new TwitterFactory(cb.build()).getInstance();

    }
}
