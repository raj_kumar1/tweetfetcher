/**
 * This class fetch top n tweets from twitter based on the query passed.
 *
 * 	@author	Raj Kumar
 */

import Constants.TwitterConstants;
import provider.TwitterProvider;
import twitter4j.*;

import java.util.Map;
import java.util.Scanner;

public class Client {

    /**
     * Replace newlines and tabs in text with escaped versions to making printing cleaner
     *
     * @param text	The text of a tweet, sometimes with embedded newlines and tabs
     * @return		The text passed in, but with the newlines and tabs replaced
     */
    public static String cleanText(String text)
    {
        text = text.replace("\n", "\\n");
        text = text.replace("\t", "\\t");

        return text;
    }

    public static void main(String[] args)
    {
        Scanner myObj = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Enter Query");

        String query = myObj.nextLine();
        System.out.println("Enter number of tweets to fetch");
        String countOfTweets = myObj.nextLine();
        int noOfTweets = 0;
        try {
            noOfTweets = Integer.parseInt(countOfTweets);
        } catch(NumberFormatException | NullPointerException e) {
            System.out.println("Invalid input. Please provide correct input.");
            System.exit(0);
        }
        if(noOfTweets <=0) {
            System.out.println("Invalid input. Number of tweets is less than or equal to zero. Please provide correct input.");
            System.exit(0);
        }
        int	totalTweets = 0;

        // to retrieve query backwards as per time.
        long maxID = -1;
        TwitterProvider twitterProvider = new TwitterProvider();
        Twitter twitter = twitterProvider.provideTwitter();

        //	Now do a simple search to show that the tokens work
        try
        {
            //Get All rate limits
            Map<String, RateLimitStatus> rateLimitStatus = twitter.getRateLimitStatus("search");
            RateLimitStatus searchTweetsRateLimit = rateLimitStatus.get("/search/tweets");

            printRateLimits(searchTweetsRateLimit);

            if(noOfTweets > searchTweetsRateLimit.getLimit()) {
                System.out.println("Maximum "+searchTweetsRateLimit.getRemaining()+" tweets can be fetched. So, fetching "+searchTweetsRateLimit.getRemaining()+" tweets.");
                noOfTweets = searchTweetsRateLimit.getRemaining();
            }
            int queryCount = noOfTweets / TwitterConstants.MAX_TWEETS_PER_QUERY;
            int tweetsToFetch = TwitterConstants.MAX_TWEETS_PER_QUERY;

            //	To retrieve tweets in batches
            for (int queryNumber=0;queryNumber <= queryCount; queryNumber++)
            {
                if(noOfTweets-(TwitterConstants.MAX_TWEETS_PER_QUERY)*queryNumber < 100) {
                    tweetsToFetch = noOfTweets-(TwitterConstants.MAX_TWEETS_PER_QUERY)*queryNumber;
                }
                handleIfLimitReached(searchTweetsRateLimit);
                Query q = getQuery(query, maxID, tweetsToFetch);
                // Make call to get tweets
                QueryResult r = twitter.search(q);
                if (r.getTweets().size() == 0)
                {
                    break;			// No tweets to fetch
                }

                for (Status s: r.getTweets())				// Loop through all the tweets...
                {
                    //increase this as we are now getting tweets
                    totalTweets++;
                    if (maxID == -1 || s.getId() < maxID)
                    {
                        maxID = s.getId();
                    }
                    System.out.printf("At %s, @%-20s said:  %s\n", s.getCreatedAt().toString(), s.getUser().getScreenName(), cleanText(s.getText()));
                }
                searchTweetsRateLimit = r.getRateLimitStatus();
            }
        }
        catch (Exception e)
        {
            System.out.println("Got exception while fetching tweets "+e);
        }

        System.out.println("Total tweets retrieved="+totalTweets);
    }

    private static void printRateLimits(RateLimitStatus searchTweetsRateLimit) {
        System.out.println("Remaining tweets fetch limit="+searchTweetsRateLimit.getRemaining());
        System.out.println("Total tweets fetch limit="+searchTweetsRateLimit.getLimit());
        System.out.println("Seconds until reset="+searchTweetsRateLimit.getSecondsUntilReset());
    }

    private static void handleIfLimitReached(RateLimitStatus searchTweetsRateLimit) throws InterruptedException {
        //	sleep if limits have reached.
        if (searchTweetsRateLimit.getRemaining() == 0)
        {
            System.out.printf("!!! Sleeping for %d seconds due to remaining tweets that can be fetched to be zero\n", searchTweetsRateLimit.getSecondsUntilReset());
            Thread.sleep((searchTweetsRateLimit.getSecondsUntilReset()+2) * 1000l);
        }
    }

    private static Query getQuery(String query, long maxID, int tweetsToFetch) {
        Query q = new Query(query);
        q.setCount(tweetsToFetch);
        q.resultType(Query.ResultType.recent);
        q.setLang("en");

        if (maxID != -1)
        {
            q.setMaxId(maxID - 1);
        }
        return q;
    }
}