package factory;

import Constants.TwitterConstants;
import twitter4j.TwitterFactory;
import twitter4j.auth.OAuth2Token;
import twitter4j.conf.ConfigurationBuilder;

public class OAuth2TokenFactory {
    /**
     * Get token to make calls
     *
     * @return	The oAuth2 bearer token
     */
    public OAuth2Token getOAuth2Token()
    {
        OAuth2Token token = null;
        ConfigurationBuilder cb;

        cb = new ConfigurationBuilder();
        cb.setApplicationOnlyAuthEnabled(true);

        cb.setOAuthConsumerKey(TwitterConstants.CONSUMER_KEY).setOAuthConsumerSecret(TwitterConstants.CONSUMER_SECRET);

        try
        {
            token = new TwitterFactory(cb.build()).getInstance().getOAuth2Token();
        }
        catch (Exception e)
        {
            System.out.println("Got exception while getting OAuth2 token"+e);
            System.exit(0);
        }

        return token;
    }
}
